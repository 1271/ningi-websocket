use std::time::{SystemTime, UNIX_EPOCH};

use array_tool::vec::Intersect;

pub mod config;
pub mod counters_util;
pub mod logger_util;
// pub mod new_release_notify;
pub mod redis_util;

pub fn get_timestamp() -> usize {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs() as usize
}

pub fn seconds_to_dhms(time_value: usize) -> String {
    let hours_all = time_value / 3600;
    let days: usize = hours_all / 24;
    let hours: usize = hours_all - days * 24;
    let minutes: usize = time_value / 60 - days * 24 * 60 - hours * 60;
    let seconds: usize = time_value % 60;

    format!("{} {:02}:{:02}:{:02}", days, hours, minutes, seconds)
}

pub async fn filter_topics(
    client_topics: &Vec<String>,
    allowed_topics: Option<Vec<String>>,
) -> Vec<String> {
    match allowed_topics.clone() {
        Some(topics) => client_topics.intersect(topics),
        None => client_topics.to_vec(),
    }
}
