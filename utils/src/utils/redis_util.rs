use std::sync::Arc;

use itertools::Itertools;
use log;
use redis::{Client, Commands, ConnectionLike};
use serde_json::{from_str, to_string};
use tokio::{sync::RwLock, time};

use crate::{config_struct::AppConfigRedis, utils::get_timestamp};

const REDIS_PREFIX: &str = "NINGI_MSG_";
const REDIS_CONNECTION_ERROR_MESSAGE: &str = "Cannot connect to redis db";
const CHECK_ITEMS_TIMEOUT: u64 = 5;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct SavedMessage {
    pub timestamp: usize,
    pub exp: usize,
    pub sender_id: Option<String>,
    pub topic: String,
    pub message: String,
}

impl SavedMessage {
    pub fn new(sender_id: Option<String>, topic: String, message: String, ttl: usize) -> Self {
        let timestamp = get_timestamp();
        let exp = get_timestamp() + ttl;

        SavedMessage {
            timestamp,
            exp,
            sender_id,
            topic,
            message,
        }
    }
}

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct SavedMessagesList {
    items: Vec<SavedMessage>,
}

impl SavedMessagesList {
    fn from_vec_ref(items: Vec<&SavedMessage>) -> Self {
        let items_ = items.iter().map(|&x| x.clone()).collect_vec();

        Self { items: items_ }
    }
    fn from_vec(items: Vec<SavedMessage>) -> Self {
        Self { items }
    }
    pub fn items(self) -> Vec<SavedMessage> {
        self.items.clone()
    }
}

pub type RedisClient = Arc<RwLock<Client>>;

pub async fn connect(config: Option<AppConfigRedis>) -> Option<RedisClient> {
    if let Some(config) = config.clone() {
        let dsn = config.dsn.clone();

        match Client::open(dsn.clone()) {
            Ok(c) => {
                if c.is_open() {
                    log::info!(r#"Connected success to "{}""#, dsn.clone());
                } else {
                    log::warn!(r#"Connected error to "{}""#, dsn.clone());
                }

                Some(Arc::new(RwLock::new(c)))
            }
            Err(e) => {
                log::warn!("{}\n{}", REDIS_CONNECTION_ERROR_MESSAGE, e.to_string());

                None
            }
        }
    } else {
        None
    }
}

async fn try_reconnect(client: RedisClient, config: AppConfigRedis) -> bool {
    if client.read().await.is_open() {
        return true;
    }

    match connect(Some(config)).await {
        Some(connection) => {
            let mut data = client.write().await;
            let new_data = connection.read().await.clone();

            *data = new_data;

            log::info!("Connection with redis restored");

            true
        }
        _ => false,
    }
}

pub async fn save_message(
    client: RedisClient,
    config: AppConfigRedis,
    receiver_id: String,
    message: SavedMessage,
) {
    let key = format!("{}{}", REDIS_PREFIX, receiver_id.clone());
    let _now = get_timestamp();

    if message.exp <= _now {
        return;
    }

    try_reconnect(client.clone(), config.clone()).await;

    let data: SavedMessagesList = match client.read().await.get_connection() {
        Ok(mut c) => match c.exists(key.clone()) {
            Ok(b) => match b {
                true => match c.get::<String, String>(key.clone().to_string()) {
                    Ok(o) => match from_str::<SavedMessagesList>(o.as_str()) {
                        Ok(x) => x,
                        Err(e) => {
                            log::warn!(
                                "Parse saved messages error. Init new data\n{}",
                                e.to_string()
                            );
                            SavedMessagesList::default()
                        }
                    },
                    Err(e) => {
                        log::warn!("Get saved messages error. Init new data\n{}", e.to_string());
                        SavedMessagesList::default()
                    }
                },
                false => {
                    log::debug!("Key {} not exists in redis db", key.clone());
                    SavedMessagesList::default()
                }
            },
            Err(_e) => {
                log::warn!("Exists command error");
                SavedMessagesList::default()
            }
        },
        Err(e) => {
            log::warn!("{}", e.to_string());
            SavedMessagesList::default()
        }
    };

    let mut actual_items: Vec<SavedMessage> = data
        .items
        .iter()
        .filter(|x| x.exp.clone() > _now.clone())
        .map(|x| x.clone())
        .collect_vec();

    actual_items.push(message);

    match client.write().await.get_connection() {
        Ok(mut c) => {
            let _: () = match c.set(
                key.clone(),
                to_string(&SavedMessagesList::from_vec(actual_items)).unwrap(),
            ) {
                Ok(o) => {
                    log::debug!("Message saved to redis db (user_id: {})", receiver_id);
                    o
                }
                Err(e) => {
                    log::warn!("Message not saved to redis db\n{}", e.to_string());
                }
            };
        }
        Err(e) => {
            log::error!("{}\n{}", REDIS_CONNECTION_ERROR_MESSAGE, e.to_string());
        }
    }
}

pub async fn get_messages(
    client: RedisClient,
    config: AppConfigRedis,
    user_id: String,
    topics: Vec<String>,
) -> String {
    try_reconnect(client.clone(), config.clone()).await;

    let key = format!("{}{}", REDIS_PREFIX, user_id);

    let result = match client.read().await.get_connection() {
        Ok(mut c) => match c.exists(key.clone()) {
            Ok(b) => match b {
                true => match c.get::<String, String>(key.clone()) {
                    Ok(m) => Ok(m),
                    Err(e) => {
                        log::error!("Get messages error\n{}", e.to_string());
                        Err(String::new())
                    }
                },
                false => {
                    log::debug!("Key {} not exists in redis db", key.clone());
                    Err(String::new())
                }
            },
            Err(e) => {
                log::warn!("Exists command error\n{}", e.to_string());
                Err(String::new())
            }
        },
        Err(e) => {
            log::error!("{}\n{}", REDIS_CONNECTION_ERROR_MESSAGE, e.to_string());
            Err(String::new())
        }
    };

    let mut to_send = vec![];

    match result {
        Ok(o) => {
            let now_ = get_timestamp();

            // перебираем сохраненные сообщения, удаляем протухшие, добавляем новые, сохраняем
            match from_str::<SavedMessagesList>(o.clone().as_str()) {
                Ok(mut l) => {
                    let items = l
                        .items
                        .iter_mut()
                        .filter(|x| x.exp > now_)
                        .map(|x| {
                            if topics.clone().contains(&x.clone().topic) && x.exp > now_ + 10 {
                                x.exp = now_ + 10;
                            }
                            &*x
                        })
                        .collect_vec();

                    to_send = Vec::from(items.clone())
                        .iter_mut()
                        .filter(|x| topics.clone().contains(&x.topic.clone()))
                        .map(|x| x.clone())
                        .collect_vec();

                    match client.write().await.get_connection() {
                        Ok(mut c) => {
                            let _: () = c
                                .set(
                                    key.clone(),
                                    to_string(&SavedMessagesList::from_vec_ref(items)).unwrap(),
                                )
                                .unwrap();
                        }
                        Err(e) => {
                            log::error!("{}\n{}", REDIS_CONNECTION_ERROR_MESSAGE, e.to_string())
                        }
                    };
                }
                Err(e) => log::error!("{}", e.to_string()),
            }
        }
        Err(_) => {}
    };

    to_string(&SavedMessagesList::from_vec(to_send)).unwrap()
}

pub async fn actualize_items(client: RedisClient, config: AppConfigRedis) {
    try_reconnect(client.clone(), config.clone()).await;

    // забираем ключи
    let keys: Vec<String> = match client.read().await.get_connection() {
        Ok(mut c) => match c.keys(format!("{}*", REDIS_PREFIX)) {
            Ok(keys) => keys,
            _ => vec![],
        },
        _ => vec![],
    };

    if 0 == keys.len() {
        return;
    }

    let _now = get_timestamp();

    // забираем данные по ключам
    let data = match client.read().await.get_connection() {
        Ok(mut c) => {
            let mut data: Vec<Option<String>> = vec![];

            for key in keys.clone() {
                match c.get(key) {
                    Ok(k) => data.push(Some(k)),
                    Err(e) => {
                        log::warn!("Get data error. Push none\n{}", e.to_string());
                        data.push(None)
                    }
                };
            }

            data
        }
        Err(e) => {
            log::error!("Connection error. Return empty data\n{}", e.to_string());
            vec![]
        }
    };

    // фильтруем
    for k in 0..(keys.len()) {
        if data.len() <= k {
            // если вдруг каких-то данных не хватает, прерываем цикл
            break;
        }

        if data[k].clone().is_none() {
            continue;
        }

        let saved_data = data[k].clone().unwrap();

        let _ = match from_str::<SavedMessagesList>(saved_data.as_str()) {
            Ok(x) => {
                let result: Vec<&SavedMessage> = x
                    .items
                    .iter()
                    .filter(|x| x.exp.clone() > _now.clone())
                    .collect_vec();

                match client.write().await.get_connection() {
                    Ok(mut c) => {
                        let _: () = match c.set(
                            &keys[k].clone(),
                            to_string(&SavedMessagesList::from_vec_ref(result)).unwrap(),
                        ) {
                            Ok(o) => o,
                            _ => (),
                        };
                    }
                    Err(e) => {
                        log::warn!("{}\n{}", REDIS_CONNECTION_ERROR_MESSAGE, e.to_string());
                    }
                }
            }
            Err(e) => {
                log::warn!("Failed parse saved data for key {}\n{}", k, e.to_string());
            }
        };
    }
}

pub async fn auto_actualize_items(client: RedisClient, config: AppConfigRedis) {
    log::info!("Init auto remover (saved messages)");

    loop {
        time::sleep(time::Duration::from_secs(CHECK_ITEMS_TIMEOUT)).await;

        actualize_items(client.clone(), config.clone()).await;
    }
}
