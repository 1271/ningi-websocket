use std::sync::Arc;
use std::sync::RwLock;
use std::{
    env,
    fs::{create_dir_all, OpenOptions},
    io::Write,
};
use tokio::time;

use env_logger::Builder;
use log::{Level, LevelFilter};

struct LogCollector {
    access: Vec<String>,
    error: Vec<String>,
}

impl LogCollector {
    fn clear(&mut self) {
        self.access.clear();
        self.error.clear();
    }
}

type LogCollectorType = Arc<RwLock<LogCollector>>;

pub async fn init_logger(log_location: Option<String>) -> Result<(), ()> {
    let env_level = env::var("WS_LOG_LEVEL")
        .unwrap_or("".to_string())
        .to_lowercase();

    let level = match env_level.trim() {
        "trace" => LevelFilter::Trace,
        "debug" => LevelFilter::Debug,
        "info" => LevelFilter::Info,
        "warn" | "warning" => LevelFilter::Warn,
        "error" => LevelFilter::Error,
        _ => LevelFilter::Warn,
    };

    let log_collector: LogCollectorType = Arc::new(RwLock::new(LogCollector {
        access: vec![],
        error: vec![],
    }));

    let _collector = Arc::clone(&log_collector.clone());

    let log_location_ = log_location.clone();
    tokio::spawn(async move { logger_task(_collector, log_location_).await });

    let log_file = if let Some(log_dir) = log_location.clone() {
        if !create_dir_all(log_dir.clone()).is_ok() {
            eprintln!(
                r#"Cannot be create log dir "{}". Interrupt"#,
                log_dir.clone()
            );

            return Err(());
        }

        Some(log_dir)
    } else {
        None
    };

    let has_log_file = log_file.clone().is_some();

    Builder::new()
        .filter(None, level)
        .format(move |f, record| {
            let raw_level = record.level();
            let level = text_level(raw_level);

            // на всякий случай дублируем ошибки прямо на экран
            match raw_level {
                Level::Error => {
                    writeln!(
                        f,
                        "{} {} {} > {}",
                        f.timestamp_millis(),
                        record.target(),
                        level,
                        record.args(),
                    )?;
                }
                _ if !has_log_file => {
                    writeln!(
                        f,
                        "{} {} {} > {}",
                        f.timestamp_millis(),
                        level,
                        record.target(),
                        record.args(),
                    )?;
                }
                _ => {}
            }

            let message = format!(
                "{} {} {} > {}\n",
                f.timestamp_millis(),
                text_level(raw_level),
                record.target(),
                record.args(),
            );

            match raw_level {
                Level::Warn | Level::Error => {
                    if let Ok(mut collector) = log_collector.clone().write() {
                        collector.error.push(message);
                    }
                }
                _ => {
                    if let Ok(mut collector) = log_collector.clone().write() {
                        collector.access.push(message);
                    }
                }
            };

            Ok(())
        })
        .init();

    Ok(())
}

fn write_log(file: String, data: Vec<String>) {
    match OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open(file.clone())
    {
        Ok(mut f) => {
            data.iter().for_each(|x| {
                f.write(x.as_bytes()).ok();
            });

            match f.flush() {
                Ok(_) => {}
                Err(e) => {
                    eprintln!(
                        "Flush log file error! {:?}\nFile: {} / Data: {:?}",
                        e, file, data
                    )
                }
            }
        }
        _ => {}
    }
}

fn text_level<'a>(level: Level) -> &'a str {
    match level {
        Level::Trace => "TRACE",
        Level::Debug => "DEBUG",
        Level::Info => "INFO",
        Level::Warn => "WARN",
        Level::Error => "ERROR",
    }
}

async fn logger_task(_collector: LogCollectorType, log_location: Option<String>) {
    loop {
        let mut access = vec![];
        let mut error = vec![];

        match _collector.clone().read() {
            Ok(data) => {
                access = Vec::clone(&data.access);
                error = Vec::clone(&data.error);
            }
            Err(_e) => {}
        }

        if let Ok(mut collector) = _collector.write() {
            collector.clear();
        }

        if let Some(log_file) = log_location.clone() {
            if error.clone().len() > 0 {
                let file = format!("{}/error.log", log_file);

                write_log(file, error.clone());
            }
            if access.clone().len() > 0 {
                let file = format!("{}/access.log", log_file);

                write_log(file, access.clone());
            }
        }

        time::sleep(time::Duration::from_secs(1)).await;
    }
}
