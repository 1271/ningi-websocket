use std::collections::HashMap;
use std::sync::Arc;

use tokio::sync::RwLock;
use uuid::Uuid;

use crate::utils::get_timestamp;

pub const MAX_RPS_COUNTER_SECONDS_HISTORY: usize = 3600;

#[derive(Clone, Debug)]
pub struct RpsCounter {
    // количество эвентов в секунду (/mps/*)
    time: usize,    // секунда, в которой произошло действие
    counter: usize, // количество произошедших действий
    minute: usize,  // минута, в которой произошло действие (timestamp / 60)
}

#[derive(Clone, Serialize, Debug, Default)]
pub struct EventsCounter {
    // количество эвентов за всю жизнь приложения (/events)
    incoming_events: usize, // количество пришедших сообщений на /publish
    missing_events: usize, // количество сообщений, которые были отправлены на топики без получателей
    outgoing_events: usize, // количество сообщений, отправленных пользователям
}

#[derive(Clone, Serialize, Debug, Default)]
pub struct UserEventsCounter {
    incoming_user_to_user_events_real: usize, // количество сообщений, полученных пользователем
    outgoing_user_to_user_events_real: usize, // количество сообщений, доставленных другим пользователям, отправленных этим пользователем
}

impl RpsCounter {
    fn reset_counter(&mut self) {
        self.counter = 0
    }
    fn inc(&mut self, inc_to: usize) {
        self.counter += inc_to
    }
    fn update_minute(&mut self, minute: usize) {
        self.minute = minute
    }
    fn new(time: usize) -> Self {
        Self {
            time,
            counter: 0,
            minute: 0,
        }
    }
}

impl EventsCounter {
    pub fn increase_incoming(&mut self) {
        self.incoming_events += 1
    }
    pub fn increase_missing(&mut self) {
        self.missing_events += 1
    }
    pub fn increase_outgoing(&mut self, inc_to: usize) {
        self.outgoing_events += inc_to
    }
}

impl UserEventsCounter {
    pub fn increase_user_to_user_incoming_real(&mut self, inc_to: usize) {
        self.incoming_user_to_user_events_real += inc_to
    }
    pub fn increase_user_to_user_outgoing_real(&mut self, inc_to: usize) {
        self.outgoing_user_to_user_events_real += inc_to
    }

    pub fn get_incoming_user_to_user_events_real(&self) -> usize {
        self.incoming_user_to_user_events_real
    }
    pub fn get_outgoing_user_to_user_events_real(&self) -> usize {
        self.outgoing_user_to_user_events_real
    }
}

pub type WsRpsCounter = Arc<RwLock<Vec<RpsCounter>>>;
pub type WsEventsCounter = Arc<RwLock<EventsCounter>>;
pub type WsUserEventsCounter = Arc<RwLock<HashMap<Uuid, UserEventsCounter>>>; // userId: events

pub async fn init_rps_counter() -> WsRpsCounter {
    let mut vec = Vec::with_capacity(MAX_RPS_COUNTER_SECONDS_HISTORY);

    for i in 0..(1 + MAX_RPS_COUNTER_SECONDS_HISTORY) {
        vec.push(RpsCounter::new(i))
    }

    let counter: WsRpsCounter = Arc::new(RwLock::new(vec));

    counter
}

pub async fn init_events_counter() -> WsEventsCounter {
    Arc::new(RwLock::new(EventsCounter::default()))
}

pub async fn init_user_events_counter() -> WsUserEventsCounter {
    Arc::new(RwLock::new(HashMap::new()))
}

pub async fn increase_rps_counter(counter: WsRpsCounter, inc_to: usize) {
    if inc_to == 0 {
        return;
    }

    let now = get_timestamp();
    let minute = now / MAX_RPS_COUNTER_SECONDS_HISTORY;
    let time = now % MAX_RPS_COUNTER_SECONDS_HISTORY;

    counter
        .write()
        .await
        .iter_mut()
        .filter(|cnt| time == cnt.time)
        .for_each(|cnt| {
            if cnt.minute != minute {
                cnt.reset_counter();
                cnt.update_minute(minute);
            }
            cnt.inc(inc_to);
        });
}

pub async fn get_rps_per_times(counter: &WsRpsCounter, times: usize) -> usize {
    _rps_counter(counter, get_timestamp(), times).await
}

// client_messages, incoming_messages
async fn _rps_counter(counter: &WsRpsCounter, to_time: usize, offset_left: usize) -> usize {
    counter
        .read()
        .await
        .iter()
        .filter(|&cnt| {
            // не учитываем последнюю секунду ради "плавности" счетчиков
            let time = 1 + cnt.time + cnt.minute * MAX_RPS_COUNTER_SECONDS_HISTORY;
            to_time >= time && to_time <= time + offset_left
        })
        .map(|cnt| cnt.counter)
        .sum()
}
