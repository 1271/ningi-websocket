use crate::config_struct::{AppConfig, AppConfigError};
use std::fs::File;
use std::io::Read;
use toml::from_str;
use validator::Validate;

pub fn load_config(path: &str) -> Result<AppConfig, AppConfigError> {
    let file = File::open(path);

    match file {
        Ok(mut f) => {
            let buf = &mut vec![];
            if !f.read_to_end(buf).is_ok() {
                return Err(AppConfigError {
                    msg: format!(r#"Config file "{}" cannot be read"#, path),
                });
            }
            let str_buf = match String::from_utf8(buf.to_vec()) {
                Ok(o) => o,
                Err(e) => {
                    return Err(AppConfigError {
                        msg: format!("{}: {}", path, e),
                    });
                }
            };
            match from_str::<AppConfig>(&str_buf) {
                Ok(o) => match o.validate() {
                    Ok(_) => Ok(o),
                    Err(e) => Err(AppConfigError {
                        msg: format!("{}: {}", path, e),
                    }),
                },
                Err(e) => Err(AppConfigError {
                    msg: format!("{}: {}", path, e),
                }),
            }
        }
        Err(e) => Err(AppConfigError {
            msg: format!("{}", e),
        }),
    }
}
