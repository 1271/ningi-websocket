use validator::Validate;

#[derive(Clone, Deserialize, Debug, Validate)]
pub struct AppConfig {
    pub daemon: AppConfigDaemon,
    #[validate(nested)]
    pub connection: AppConfigConnection,
    pub statistic: AppConfigStatistic,
    #[validate(nested)]
    pub proxy: AppConfigProxy,
    #[validate(nested)]
    pub redis: Option<AppConfigRedis>,
    #[validate(nested)]
    pub tls: Option<AppConfigDaemonTls>,
}

#[derive(Clone, Deserialize, Debug, Validate)]
pub struct AppConfigDaemonTls {
    #[validate(length(min = 1))]
    pub cert: String,
    #[validate(length(min = 1))]
    pub key: String,
}
#[derive(Clone, Deserialize, Debug, Validate)]
pub struct AppConfigProxy {
    pub ips: Option<Vec<String>>,
    #[validate(length(min = 1))]
    pub header: String,
}
#[derive(Clone, Deserialize, Debug, Validate)]
pub struct AppConfigRedis {
    pub dsn: String,
    pub ttl: u16,
}
#[derive(Clone, Deserialize, Debug)]
pub struct AppConfigDaemon {
    pub port: u16,
    pub jwt_public_key: String,
    pub internal_ip: [u8; 4],
    pub log_location: Option<String>,
    pub topics: Option<Vec<String>>,
    pub publish_key: String,
}

#[derive(Clone, Deserialize, Debug, Validate)]
pub struct AppConfigConnection {
    #[validate(length(min = 1))]
    pub ws_host: String,
    pub wss: bool,
    pub port: Option<u16>,
    pub request_timeout: u8,
    #[validate(range(min = 5, max = 120))]
    pub max_idle_time: u8,
}

#[derive(Clone, Deserialize, Debug)]
pub struct AppConfigStatistic {
    pub routes_secret: Option<String>,
}

pub struct AppConfigError {
    pub msg: String,
}
