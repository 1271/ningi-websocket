all: install-rust build

install-rust:
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o rustup.sh && chmod +x rustup.sh && ./rustup.sh -y && rm rustup.sh

update-rust:
	rustup update

build:
	cargo build --release
	strip -s target/release/ningi_ws
    upx-4.2.3-amd64_linux/upx --best --lzma target/release/ningi_ws

run:
	WS_LOG_LEVEL=info target/release/ningi_ws
