#[macro_use]
extern crate serde_derive;

mod app;

// use ningi_ws_utils::utils::new_release_notify::loop_checker;

#[tokio::main(flavor = "multi_thread", worker_threads = 10)]
async fn main() {
    let matches = app::args::get_args();

    // tokio::spawn(loop_checker());
    let config = app::prepare(matches.value_of("config")).await;
    app::app(config).await;
}
