use std::collections::HashSet;

use jsonwebtoken::errors::ErrorKind;
use jsonwebtoken::Validation;
use serde::Deserialize;
use serde_aux::prelude::deserialize_number_from_string;
use serde_aux::prelude::deserialize_option_number_from_string;

use ningi_ws_utils::utils::get_timestamp;

pub type Result<T> = std::result::Result<T, Error>;

pub struct Error(Box<ErrorKind>);

pub(crate) trait KidErr {
    fn kind(self) -> ErrorKind;
}

impl KidErr for Error {
    fn kind(self) -> ErrorKind {
        return *self.0;
    }
}

#[allow(non_snake_case)]
#[derive(Deserialize, Debug, Clone)]
pub(crate) struct Claims {
    pub(crate) aud: Option<HashSet<String>>,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub(crate) exp: f64,
    // #[serde(default, deserialize_with = "deserialize_option_number_from_string")]
    // pub(crate) iat: Option<f64>,
    pub(crate) iss: Option<String>,
    #[serde(default, deserialize_with = "deserialize_option_number_from_string")]
    pub(crate) nbf: Option<f64>,
    pub(crate) sub: Option<String>,
    pub(crate) userId: String,
    // pub(crate) roles: Option<Vec<String>>,
    // pub(crate) username: Option<String>,
}
#[warn(non_snake_case)]

pub(crate) trait ClaimsValidateTrait {
    fn validate(self, validator: Validation) -> Result<()>;
}

impl ClaimsValidateTrait for Claims {
    fn validate(self, validator: Validation) -> Result<()> {
        validate(&self, &validator)
    }
}

fn validate(claims: &Claims, options: &Validation) -> Result<()> {
    let now = get_timestamp() as u64;
    let exp_leeway = (now - options.leeway) as f64;

    if options.validate_exp {
        let exp = claims.exp.clone();

        if exp.clone() < exp_leeway {
            return Err(Error(Box::new(ErrorKind::ExpiredSignature)));
        }
    }

    if options.validate_nbf {
        if let Some(nbf) = claims.nbf.clone() {
            if nbf.clone() > exp_leeway {
                return Err(Error(Box::new(ErrorKind::ImmatureSignature)));
            }
        } else {
            return Err(Error(Box::new(ErrorKind::ImmatureSignature)));
        }
    }

    if let Some(correct_sub) = options.sub.clone() {
        if let Some(sub) = claims.sub.clone() {
            if sub != correct_sub {
                return Err(Error(Box::new(ErrorKind::InvalidSubject)));
            }
        }
    }

    if let Some(correct_iss) = options.iss.clone() {
        if let Some(iss) = claims.iss.clone() {
            if !correct_iss.contains(iss.as_str()) {
                return Err(Error(Box::new(ErrorKind::InvalidIssuer)));
            }
        } else {
            return Err(Error(Box::new(ErrorKind::InvalidIssuer)));
        }
    }

    if let Some(ref correct_aud) = options.aud.clone() {
        if let Some(aud) = claims.aud.clone() {
            if aud.intersection(correct_aud).count() == 0 {
                return Err(Error(Box::new(ErrorKind::InvalidAudience)));
            }
        } else {
            return Err(Error(Box::new(ErrorKind::InvalidAudience)));
        }
    }

    Ok(())
}
