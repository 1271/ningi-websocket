// use std::process::exit;

#[macro_export]
macro_rules! error_exit {
    ($($arg:tt)*) => {{
        eprintln!($($arg)*);
        exit(1)
    }}
}
