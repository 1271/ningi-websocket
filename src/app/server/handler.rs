use std::{
    collections::{hash_map::RandomState, HashMap, HashSet},
    ops::DerefMut,
    process::exit,
    str::FromStr,
    sync::Arc,
};

use http::HeaderValue;
use itertools::Itertools;
use jsonwebtoken::{Algorithm, DecodingKey, Validation};
use log;
use uuid::Uuid;
use warp::{
    reply::{json, with_status, Json, WithStatus},
    ws::Message,
    Reply,
};
use warp::http::StatusCode;

use crate::app::{
    jwt_struct,
    jwt_struct::{ClaimsValidateTrait, KidErr},
    ws, Client, Clients, ClientsEvents, WsMaxConnectionsType, WsResult, MAX_CONNECTIONS_PER_USER,
};
use ningi_ws_utils::{
    config_struct::AppConfig,
    utils::{counters_util, filter_topics, get_timestamp, redis_util},
};

type SimpleUserConnectionsResponse = HashMap<Uuid, Vec<String>>;

#[derive(Deserialize, Debug)]
pub(crate) struct RegisterRequest {
    token: String,
    topics: Vec<String>,
}

#[derive(Serialize, Debug)]
pub(crate) struct PublishMessage {
    message: String,
    topic: String,
}

#[derive(Serialize, Debug)]
pub(crate) struct RegisterResponse {
    url: String,
}

#[derive(Serialize, Debug)]
struct ErrorResponse {
    message: String,
}

#[derive(Serialize, Debug)]
pub(crate) struct ConnectedClientsResponse {
    count: usize,
    clients: Vec<Uuid>,
}

#[derive(Serialize, Debug)]
pub(crate) struct ConnectedClientsWithConnectionsResponse {
    count: usize,
    clients: SimpleUserConnectionsResponse,
}

#[derive(Deserialize, Debug)]
pub(crate) struct Event {
    topic: String,
    user_ids: Option<Vec<Uuid>>,
    message: String,
    key: String,
}

#[derive(Serialize, Debug)]
pub(crate) struct TopicsResponse {
    topics: Option<Vec<String>>,
}

#[derive(Serialize, Debug, Default)]
pub(crate) struct WsMpsResponse {
    mps: f64,
    count: usize,
}

#[derive(Serialize, Debug, Default)]
pub(crate) struct WsUserCountResponse {
    current_clients: usize,
    current_connections: usize,
    max_clients: usize,
    max_connections: usize,
}

#[derive(Serialize, Debug)]
pub(crate) struct UserConnectionResponse {
    connection_id: Uuid,
    connected_at: usize,
    topics: Vec<String>,
    connected: bool,
    latest_activity: Option<usize>,
    token_exp: usize,
}

#[derive(Serialize, Debug, Default)]
struct UserConnectionsResponseCounters {
    incoming_from_users: usize,
    outgoing_to_users: usize,
}

#[derive(Serialize, Debug, Default)]
pub(crate) struct UserConnectionsResponse {
    count: usize,
    user_id: Uuid,
    connections: Vec<UserConnectionResponse>,
    events: ClientEvents,
    user_events: UserConnectionsResponseCounters,
}

#[derive(Serialize, Default, Debug)]
struct ClientEvents {
    received: usize, // сообщения, полученные клиентом
    sent: usize,     // сообщения, отправленные клиентом
}

pub(crate) async fn publish_handler(
    body: Event,
    clients: Clients,
    key: String,
    counter: counters_util::WsRpsCounter,
    events_counter: counters_util::WsEventsCounter,
    redis_client: Option<redis_util::RedisClient>,
    clients_events: ClientsEvents,
    config: AppConfig,
) -> WsResult<impl Reply> {
    if !body.key.eq(&key) {
        return Ok(with_status(
            json(&ErrorResponse {
                message: "Bad key".to_string(),
            }),
            StatusCode::BAD_REQUEST,
        ));
    }

    log::debug!(
        "New incoming event. topic: {}, message: {}",
        body.topic.clone(),
        body.message.clone(),
    );

    let mut inc_to = 0;

    events_counter.write().await.deref_mut().increase_incoming();
    let mut actual_clients: Vec<Uuid> = vec![];

    let expected_clients: Vec<Uuid> = match body.user_ids.clone() {
        Some(_ids) => _ids.iter().map(|&x| x).collect(),
        _ => vec![],
    };

    let mut inc_events_to: HashMap<Uuid, usize> = HashMap::new();

    clients
        .read()
        .await
        .iter()
        .filter(|(_, client)| match body.user_ids.clone() {
            Some(ids) => ids.contains(&client.user_id),
            None => true,
        })
        .filter(|(_, client)| client.topics.contains(&body.topic))
        .for_each(|(_, client)| {
            if let Some(sender) = &client.sender.clone() {
                actual_clients.push(client.user_id.clone());

                let message = serde_json::to_string(&PublishMessage {
                    message: body.message.clone(),
                    topic: body.topic.clone(),
                })
                .unwrap();

                let current = *inc_events_to
                    .get(&client.user_id.clone())
                    .unwrap_or(&0usize);

                inc_events_to.insert(client.user_id.clone(), 1usize + current);

                inc_to += 1;

                let _ = sender.send(Ok(Message::text(message)));
            }
        });

    log::debug!("Pre-increase events received counter");

    for (uid, counter) in inc_events_to {
        if counter > 0 {
            clients_events
                .clone()
                .write()
                .await
                .get_mut(&uid.clone())
                .unwrap()
                .received_events += counter;
        }
    }

    // region offline_save
    // находим клиентов, которым не пришло сообщение и сохраняем для них это сообщение (оффлайн)
    let mut _expected_hash: HashSet<Uuid, RandomState> = HashSet::new();
    _expected_hash.extend(expected_clients.clone().iter());

    let mut _actual_hash: HashSet<Uuid, RandomState> = HashSet::new();
    _actual_hash.extend(actual_clients.clone().iter());

    let _diff = _expected_hash
        .difference(&_actual_hash)
        .map(|&x| x)
        .collect_vec();

    if let Some(client) = redis_client {
        let redis_config = config.redis.clone();
        if let Some(redis_config) = redis_config {
            let ttl = redis_config.ttl.clone();

            if ttl > 0 {
                log::debug!("Pre-save messages to redis");

                let ttl = ttl as usize;

                for uid in _diff.clone() {
                    redis_util::save_message(
                        client.clone(),
                        redis_config.clone(),
                        uid.to_string(),
                        redis_util::SavedMessage::new(
                            None,
                            body.topic.clone(),
                            body.message.clone(),
                            ttl,
                        ),
                    )
                    .await;
                }
            }
        }
    }
    // endregion offline_save

    if inc_to > 0 {
        log::debug!("Pre-increase events outgoing counter");

        events_counter
            .write()
            .await
            .deref_mut()
            .increase_outgoing(inc_to);
    } else {
        log::debug!("Pre-increase events missing counter");

        events_counter.write().await.deref_mut().increase_missing();
    }

    counters_util::increase_rps_counter(Arc::clone(&counter), inc_to).await;

    Ok(with_status(json(&{}), StatusCode::OK))
}

pub(crate) async fn register_handler(
    body: RegisterRequest,
    clients: Clients,
    ws_host: String,
    buffer: Vec<u8>,
    config: AppConfig,
) -> WsResult<impl Reply> {
    let mut validation = Validation::new(Algorithm::RS256);
    validation.algorithms = vec![Algorithm::RS256, Algorithm::RS384, Algorithm::RS512];

    let decoding_key = match DecodingKey::from_rsa_pem(&buffer) {
        Ok(key) => key,
        Err(e) => {
            log::error!("Bad server JWT key!");
            log::error!("{}", e.to_string());

            exit(1)
        }
    };

    let token = body.token.clone();

    let token_data = match jsonwebtoken::decode::<jwt_struct::Claims>(
        token.as_str(), &decoding_key.clone(), &validation.clone()
    ) {
        Ok(o) => o,
        Err(err) => {
            log::debug!(r#"JWT payload has bad. {:?}"#, err);

            return Ok(with_status(
                json(&ErrorResponse {
                    message: err.to_string(),
                }),
                StatusCode::BAD_REQUEST,
            ));
        }
    };

    let claims = token_data.claims;

    match claims.clone().validate(validation) {
        Ok(_) => {}
        Err(err) => {
            let kid = err.kind();

            log::debug!(r#"JWT validate error. {:?}"#, kid);

            return Ok(with_status(
                json(&ErrorResponse {
                    message: format!("{:?}", kid),
                }),
                StatusCode::BAD_REQUEST,
            ));
        }
    };

    if body.topics.len() == 0 {
        log::info!(r#"Bad user data. "topics" cannot be empty"#);

        return Ok(with_status(
            json(&ErrorResponse {
                message: r#"Bad data. "topics" cannot be empty"#.to_string(),
            }),
            StatusCode::BAD_REQUEST,
        ));
    }

    let user_id = Uuid::parse_str(&claims.clone().userId).unwrap();
    let own_connects = Arc::clone(&clients.clone())
        .read()
        .await
        .iter()
        .filter(|(_, client)| {
            (client.is_connected.clone() || client.exp_at() > get_timestamp())
                && client.user_id.clone() == user_id.clone()
        })
        .count();

    if own_connects >= MAX_CONNECTIONS_PER_USER {
        log::warn!("Connections limit for user: {}", user_id);

        return Ok(with_status(
            json(&ErrorResponse {
                message: "Connections limit".to_string(),
            }),
            StatusCode::TOO_MANY_REQUESTS,
        ));
    }

    let response = pre_register_client(body, clients.clone(), ws_host, claims, config).await;

    Ok(response)
}

async fn pre_register_client(
    body: RegisterRequest,
    clients: Clients,
    ws_host: String,
    claims: jwt_struct::Claims,
    config: AppConfig,
) -> WithStatus<Json> {
    let uuid = Uuid::new_v4().to_string();

    let ws_schema = if config.connection.wss.clone() {
        "wss"
    } else {
        "ws"
    };

    if let Ok(user_id) = Uuid::from_str(claims.userId.as_str()) {
        register_client(
            uuid.clone(),
            user_id,
            body.topics,
            clients,
            claims.exp as usize, // игнорируем доли секунды
            config,
        )
        .await;

        log::info!(
            r#"Register new client. Uid: "{}". ConnectionUrlId: "{}""#,
            claims.userId,
            uuid.clone(),
        );

        with_status(
            json(&RegisterResponse {
                url: format!("{}://{}/{}", ws_schema, ws_host, uuid),
            }),
            StatusCode::OK,
        )
    } else {
        log::info!(
            r#"Bad claims user data. "userId" not present. ConnectionUrlId: "{}""#,
            uuid
        );

        with_status(
            json(&ErrorResponse {
                message: String::from(r#"Bad user data. "userId" not present"#),
            }),
            StatusCode::BAD_REQUEST,
        )
    }
}

async fn register_client(
    id: String,
    user_id: Uuid,
    client_topics: Vec<String>,
    clients: Clients,
    token_exp: usize,
    config: AppConfig,
) {
    let topics = filter_topics(&client_topics, config.daemon.topics.clone()).await;

    log::debug!("Pre-register new ws client {}", id.clone());

    let timestamp = get_timestamp();

    let timeout = config.connection.request_timeout.clone() as usize;

    clients.write().await.insert(
        id.clone(),
        Client {
            user_id,
            topics,
            sender: None,
            timeout,
            token_exp,
            latest_activity: None,
            connected_at: timestamp,
            is_connected: false,
        },
    );
}

pub(crate) async fn unregister_handler(id: String, clients: Clients) -> WsResult<impl Reply> {
    log::debug!("Pre-unregister new ws client {} unregistered", id.clone());

    clients.write().await.remove(&id);
    Ok(StatusCode::OK)
}

pub(crate) async fn ws_handler(
    connection_id: Uuid,
    ws: warp::ws::Ws,
    clients: Clients,
    config: AppConfig,
    max_connections: WsMaxConnectionsType,
    redis_client: Option<redis_util::RedisClient>,
    clients_events: ClientsEvents,
    user_events_counter: counters_util::WsUserEventsCounter,
) -> WsResult<impl Reply> {
    let client = clients
        .read()
        .await
        .get(&connection_id.clone().to_string())
        .cloned();

    match client {
        Some(c) => {
            log::info!(
                "Handle new ws client {}. ConnectionUrlId: {}",
                c.user_id.clone(),
                connection_id.clone().to_string(),
            );

            let response = ws.on_upgrade(move |socket| {
                ws::client_connection(
                    socket,
                    connection_id,
                    clients,
                    c,
                    config.clone(),
                    max_connections,
                    redis_client.clone(),
                    clients_events.clone(),
                    user_events_counter.clone(),
                )
            });
            Ok(Reply::into_response(response))
        }
        None => {
            log::warn!("Connection {} not have client", connection_id.to_string());

            Err(warp::reject::not_found())
        }
    }
}

pub(crate) async fn health_handler() -> WsResult<impl Reply> {
    Ok(StatusCode::NO_CONTENT)
}

pub(crate) async fn get_clients_handler(
    clients: Clients,
    header_secret: Option<HeaderValue>,
    secret: Option<String>,
) -> WsResult<impl Reply> {
    let status;

    let _clients: Vec<Uuid> = if check_stat_header(header_secret, secret) {
        status = StatusCode::OK;

        clients
            .read()
            .await
            .iter()
            .filter(|(_, client)| client.is_connected)
            .map(|(_, client)| client.user_id)
            .unique()
            .collect()
    } else {
        status = StatusCode::FORBIDDEN;

        vec![]
    };

    Ok(with_status(
        json(&ConnectedClientsResponse {
            count: _clients.len(),
            clients: _clients,
        }),
        status,
    ))
}

pub(crate) async fn get_clients_handler_with_connections(
    clients: Clients,
    header_secret: Option<HeaderValue>,
    secret: Option<String>,
) -> WsResult<impl Reply> {
    let status;

    let _clients: SimpleUserConnectionsResponse = if check_stat_header(header_secret, secret) {
        let mut _clients: SimpleUserConnectionsResponse = HashMap::new();
        status = StatusCode::OK;

        clients
            .read()
            .await
            .iter()
            .filter(|(_, client)| client.is_connected)
            .for_each(|(cid, client)| {
                if _clients.contains_key(&client.user_id.clone()) {
                    if let Some(mut _client) = _clients.get_mut(&client.user_id.clone()) {
                        _client.push(cid.clone());
                    }
                } else {
                    _clients.insert(client.user_id.clone(), vec![cid.clone()]);
                }
            }); // узкое место. В будущем добавить ограничения на получение информации

        _clients
    } else {
        status = StatusCode::FORBIDDEN;

        HashMap::default()
    };

    Ok(with_status(
        json(&ConnectedClientsWithConnectionsResponse {
            count: _clients.len(),
            clients: _clients,
        }),
        status,
    ))
}

pub(crate) async fn get_clients_count_handler(
    clients: Clients,
    _max_connections: WsMaxConnectionsType,
    header_secret: Option<HeaderValue>,
    secret: Option<String>,
) -> WsResult<impl Reply> {
    if !check_stat_header(header_secret, secret) {
        return Ok(with_status(
            json(&WsUserCountResponse::default()),
            StatusCode::FORBIDDEN,
        ));
    }

    let current_clients = clients
        .read()
        .await
        .iter()
        .map(|(_, client)| client.user_id)
        .unique()
        .count();

    let current_connections = clients.read().await.iter().count();

    let max_clients = _max_connections.read().await.max_clients;
    let max_connections = _max_connections.read().await.max_connections;

    Ok(with_status(
        json(&WsUserCountResponse {
            current_clients,
            current_connections,
            max_clients,
            max_connections,
        }),
        StatusCode::OK,
    ))
}

pub(crate) async fn get_topics_handler(config: AppConfig) -> WsResult<impl Reply> {
    let topics = config.daemon.topics.clone();

    Ok(with_status(
        json(&TopicsResponse { topics }),
        StatusCode::OK,
    ))
}

pub(crate) async fn get_mps_handler(
    times: usize,
    counter: counters_util::WsRpsCounter,
    header_secret: Option<HeaderValue>,
    secret: Option<String>,
) -> WsResult<impl Reply> {
    if !check_stat_header(header_secret, secret) {
        return Ok(with_status(
            json(&WsMpsResponse::default()),
            StatusCode::FORBIDDEN,
        ));
    }

    let available_times: Vec<usize> = vec![
        5, 10, 30, 60, 90, 120, 180, 240, 300, 600, 900, 1200, 1500, 1800, 2400, 3000, 3600,
    ];

    if times > counters_util::MAX_RPS_COUNTER_SECONDS_HISTORY
        || !available_times.iter().any(|&x| x == times)
    {
        return Ok(with_status(
            json(&WsMpsResponse::default()),
            StatusCode::NOT_FOUND,
        ));
    }

    let count = counters_util::get_rps_per_times(&counter.clone(), times).await;
    let mps: f64 = (count as f64) / (times as f64);

    Ok(with_status(
        json(&WsMpsResponse { mps, count }),
        StatusCode::OK,
    ))
}

pub(crate) async fn get_client_connections_handler(
    client_id: Uuid,
    clients: Clients,
    clients_events: ClientsEvents,
    user_to_user_events_counter: counters_util::WsUserEventsCounter,
    header_secret: Option<HeaderValue>,
    secret: Option<String>,
) -> WsResult<impl Reply> {
    if !check_stat_header(header_secret, secret) {
        return Ok(with_status(
            json(&UserConnectionsResponse::default()),
            StatusCode::FORBIDDEN,
        ));
    }

    let connections = clients
        .read()
        .await
        .iter()
        .filter(|(_, c)| c.user_id == client_id.clone())
        .map(|(cid, c)| UserConnectionResponse {
            connected_at: c.connected_at.clone(),
            topics: c.topics.clone(),
            connection_id: Uuid::from_str(cid.as_str()).unwrap(),
            connected: c.is_connected,
            latest_activity: c.latest_activity,
            token_exp: c.token_exp,
        })
        .collect_vec();

    let user_events_counter = user_to_user_events_counter.read().await;

    let events_ = user_events_counter.get(&client_id.clone());

    let counters = match events_ {
        Some(c) => UserConnectionsResponseCounters {
            incoming_from_users: c.get_incoming_user_to_user_events_real(),
            outgoing_to_users: c.get_outgoing_user_to_user_events_real(),
        },
        None => UserConnectionsResponseCounters::default(),
    };

    match clients_events.read().await.get(&client_id.clone()) {
        Some(event) => Ok(with_status(
            json(&UserConnectionsResponse {
                count: connections.len(),
                user_id: client_id.clone(),
                connections,
                events: ClientEvents {
                    received: event.clone().received_events,
                    sent: event.clone().sent_events,
                },
                user_events: counters,
            }),
            StatusCode::OK,
        )),
        None => Ok(with_status(
            json(&UserConnectionsResponse::default()),
            StatusCode::NOT_FOUND,
        )),
    }
}

pub(crate) async fn get_events_counter(
    counter: counters_util::WsEventsCounter,
    header_secret: Option<HeaderValue>,
    secret: Option<String>,
) -> WsResult<impl Reply> {
    if !check_stat_header(header_secret, secret) {
        Ok(with_status(
            json(&counters_util::EventsCounter::default()),
            StatusCode::FORBIDDEN,
        ))
    } else {
        Ok(with_status(
            json(&counter.read().await.clone()),
            StatusCode::OK,
        ))
    }
}

// если значение присутствует и не пустое, необходимо проверить заголовок
fn check_stat_header(header_secret: Option<HeaderValue>, secret: Option<String>) -> bool {
    if let Some(secret) = secret {
        match header_secret {
            Some(h) => secret.as_bytes() == h.as_bytes(),
            _ => false,
        }
    } else {
        true
    }
}
