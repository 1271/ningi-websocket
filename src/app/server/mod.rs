use warp::Filter;

use ningi_ws_utils::{
    config_struct::AppConfig,
    utils::counters_util::{WsEventsCounter, WsUserEventsCounter},
    utils::{counters_util::WsRpsCounter, get_timestamp, redis_util::RedisClient},
};

use crate::app::{Clients, ClientsEvents, WsMaxConnectionsType};
use std::sync::Arc;
use warp::ws::Message;

mod handler;
mod routes;

pub(crate) async fn run_server(
    clients: Clients,
    buffer: Vec<u8>,
    counter: WsRpsCounter,
    events_counter: WsEventsCounter,
    user_events_counter: WsUserEventsCounter,
    max_connections: WsMaxConnectionsType,
    redis_client: Option<RedisClient>,
    clients_events: ClientsEvents,
    config: AppConfig,
) {
    let starting_time = get_timestamp();
    let proxy_config = config.proxy.clone();

    let custom_log = warp::log::custom(move |info| {
        let undefined = String::from("undefined");

        let remote_addr = match info.remote_addr() {
            Some(ip) => ip.to_string(),
            _ => undefined.clone(),
        };

        let header_value = info.request_headers().get(proxy_config.header.clone());

        let ip = if let Some(ips) = proxy_config.ips.clone() {
            match ips.iter().any(|val| *val == remote_addr) {
                true => match header_value {
                    Some(val) => match String::from_utf8(val.as_bytes().to_vec()) {
                        Ok(addr) => addr,
                        _ => undefined,
                    },
                    _ => undefined,
                },
                false => remote_addr,
            }
        } else {
            remote_addr
        };

        log::info!(
            "{} \"{} {} {:?}\" {} \"{}\" \"{}\" {:?}",
            ip,
            info.method(),
            info.path(),
            info.version(),
            info.status().as_u16(),
            match info.referer() {
                Some(x) => x,
                None => "-",
            },
            match info.user_agent() {
                Some(x) => x,
                None => "-",
            },
            info.elapsed(),
        );
    });

    let host_str = match config.connection.port.clone() {
        Some(port) => format!("{}:{}", config.connection.ws_host.clone(), port),
        None => config.connection.ws_host.clone(),
    };

    let health_route = warp::path!("health")
        .and(warp::head())
        .and_then(handler::health_handler)
        .or(warp::path!("health")
            .and(warp::get())
            .and_then(handler::health_handler));

    let clients_route =
        routes::clients_route_builder(clients.clone(), config.statistic.clone()).await;

    let clients_with_connections_route =
        routes::clients_with_connections_route_builder(clients.clone(), config.statistic.clone())
            .await;

    let topics_list_route = routes::topics_route_builder(config.clone()).await;

    let register_routes =
        routes::register_route_builder(clients.clone(), host_str, buffer, config.clone()).await;

    let publish = routes::publish_route_builder(
        clients.clone(),
        config.daemon.publish_key.clone(),
        counter.clone(),
        events_counter.clone(),
        redis_client.clone(),
        clients_events.clone(),
        config.clone(),
    )
    .await;

    let ws_route = routes::ws_route_builder(
        clients.clone(),
        config.clone(),
        max_connections.clone(),
        redis_client.clone(),
        clients_events.clone(),
        user_events_counter.clone(),
    )
    .await;

    let mps_route = routes::get_mps_builder(counter.clone(), config.statistic.clone()).await;

    let connections_per_client = routes::connections_per_client_builder(
        clients.clone(),
        clients_events.clone(),
        user_events_counter.clone(),
        config.statistic.clone(),
    )
    .await;

    let clients_count_route =
        routes::get_users_stat_builder(clients, max_connections, config.statistic.clone()).await;

    let event_counter_route =
        routes::events_counter_builder(events_counter.clone(), config.statistic.clone()).await;

    let version_route = routes::version_route_builder().await;

    let not_found_route = routes::not_found_route_builder().await;

    let uptime_route = routes::get_daemon_uptime_builder(starting_time).await;

    let routes = health_route
        .or(topics_list_route)
        .or(clients_route)
        .or(clients_with_connections_route)
        .or(register_routes)
        .or(ws_route)
        .or(publish)
        .or(mps_route)
        .or(connections_per_client)
        .or(clients_count_route)
        .or(event_counter_route)
        .or(version_route)
        .or(uptime_route)
        .or(not_found_route)
        .with(custom_log)
        .with(warp::cors().allow_any_origin());

    let addr = (
        config.daemon.internal_ip.clone(),
        config.daemon.port.clone(),
    );

    match config.tls {
        Some(tls) => {
            log::debug!(
                r#"Use TLS cert: "{}"\nUse TLS key: "{}""#,
                tls.cert.clone(),
                tls.key.clone()
            );

            warp::serve(routes)
                .tls()
                .cert_path(tls.cert.clone())
                .key_path(tls.key.clone())
                .run(addr)
                .await
        }
        _ => warp::serve(routes).run(addr).await,
    }
}

pub(crate) async fn auto_clear_dead_connections_loop(clients: Clients) {
    log::info!("Init auto remover (requests)");

    loop {
        tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;

        remove_dead_requests(Arc::clone(&clients)).await;
    }
}

pub(crate) async fn auto_disconnect_loop(clients: Clients, max_idle_time: u8) {
    log::info!("Init auto disconnect (clients)");

    let max_idle_time = max_idle_time as usize;

    loop {
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;

        close_dead_connections(Arc::clone(&clients), max_idle_time.clone()).await;
    }
}

async fn remove_dead_requests(clients: Clients) {
    let _len = clients.read().await.len();
    log::trace!("Auto requests remover > current requests: {}", _len);

    tokio::time::sleep(tokio::time::Duration::from_millis(_len as u64)).await;

    let timestamp = get_timestamp();

    clients
        .write()
        .await
        .retain(|_, client| client.clone().is_connected || client.clone().exp_at() > timestamp);

    clients.write().await.shrink_to_fit();
}

async fn close_dead_connections(clients: Clients, max_idle_time: usize) {
    let _len = clients.read().await.len();
    log::trace!("Auto clients remover > current clients: {}", _len);

    let timestamp = get_timestamp();

    for mut client in clients
        .write()
        .await
        .iter_mut()
        .filter(|(_, client)| {
            let connected_at = client.latest_activity.unwrap_or(client.connected_at);

            client.is_connected && connected_at + max_idle_time <= timestamp
        })
        .map(|(_, client)| client.clone())
    {
        if let Some(sender) = client.sender {
            sender
                .send(Ok(Message::close_with(
                    0u16,
                    String::from("Disconnected for inactivity"),
                )))
                .ok();
        }

        client.is_connected = false
    }
}
