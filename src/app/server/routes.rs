use std::sync::Arc;

use uuid::Uuid;
use warp::{http::{Response, StatusCode}, Filter, Rejection, Reply};

use crate::app::{server::handler, Clients, ClientsEvents, WsMaxConnectionsType};
use ningi_ws_utils::{
    config_struct::{AppConfig, AppConfigStatistic},
    utils::{
        counters_util::{WsEventsCounter, WsRpsCounter, WsUserEventsCounter},
        get_timestamp, redis_util, seconds_to_dhms,
    },
};

pub(crate) async fn publish_route_builder(
    clients: Clients,
    publish_secret_key: String,
    counter: WsRpsCounter,
    events_counter: WsEventsCounter,
    redis_client: Option<redis_util::RedisClient>,
    clients_events: ClientsEvents,
    config: AppConfig,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("publish")
        .and(warp::body::content_length_limit(1 << 16)) // 64kb
        .and(warp::post())
        .and(warp::body::json())
        .and(warp::any().map(move || clients.clone()))
        .and(warp::any().map(move || publish_secret_key.clone()))
        .and(warp::any().map(move || counter.clone()))
        .and(warp::any().map(move || events_counter.clone()))
        .and(warp::any().map(move || redis_client.clone()))
        .and(warp::any().map(move || clients_events.clone()))
        .and(warp::any().map(move || config.clone()))
        .and_then(handler::publish_handler)
}

pub(crate) async fn register_route_builder(
    clients: Clients,
    host_str: String,
    buffer: Vec<u8>,
    config: AppConfig,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    let register = warp::path!("register");
    let clients_register = Arc::clone(&clients);

    let register_routes = register
        .and(warp::body::content_length_limit(4096))
        .and(warp::post())
        .and(warp::body::json())
        .and(warp::any().map(move || clients.clone()))
        .and(warp::any().map(move || host_str.clone()))
        .and(warp::any().map(move || buffer.clone()))
        .and(warp::any().map(move || config.clone()))
        .and_then(handler::register_handler)
        .or(register
            .and(warp::delete())
            .and(warp::path::param())
            .and(warp::any().map(move || clients_register.clone()))
            .and_then(handler::unregister_handler));

    register_routes
}

pub(crate) async fn ws_route_builder(
    clients: Clients,
    config: AppConfig,
    max_connections: WsMaxConnectionsType,
    redis_client: Option<redis_util::RedisClient>,
    clients_events: ClientsEvents,
    user_to_user_events_counter: WsUserEventsCounter,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!(Uuid)
        .and(warp::ws())
        .and(warp::any().map(move || clients.clone()))
        .and(warp::any().map(move || config.clone()))
        .and(warp::any().map(move || max_connections.clone()))
        .and(warp::any().map(move || redis_client.clone()))
        .and(warp::any().map(move || clients_events.clone()))
        .and(warp::any().map(move || user_to_user_events_counter.clone()))
        .and_then(handler::ws_handler)
}

pub(crate) async fn not_found_route_builder(
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path::end().map(|| {
        Response::builder()
            .status(StatusCode::NOT_FOUND)
            .header("content-type", "text/html; charset=utf-8")
            .body(NOT_FOUND_CONTENT)
    })
}

pub(crate) async fn version_route_builder(
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    let version = env!("CARGO_PKG_VERSION");
    warp::path!("version")
        .and(warp::get())
        .and(warp::any().map(move || version))
        .map(|version| {
            Response::builder()
                .status(StatusCode::OK)
                .header("content-type", "text/html; charset=utf-8")
                .body(version)
        })
}

pub(crate) async fn clients_route_builder(
    clients: Clients,
    config: AppConfigStatistic,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("clients")
        .and(warp::any().map(move || clients.clone()))
        .and(warp::header::optional("Secret-Key"))
        .and(warp::any().map(move || config.routes_secret.clone()))
        .and_then(handler::get_clients_handler)
}

pub(crate) async fn clients_with_connections_route_builder(
    clients: Clients,
    config: AppConfigStatistic,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("clients" / "with-connections")
        .and(warp::any().map(move || clients.clone()))
        .and(warp::header::optional("Secret-Key"))
        .and(warp::any().map(move || config.routes_secret.clone()))
        .and_then(handler::get_clients_handler_with_connections)
}

pub(crate) async fn topics_route_builder(
    config: AppConfig,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("topics")
        .and(warp::get())
        .and(warp::any().map(move || config.clone()))
        .and_then(handler::get_topics_handler)
}

pub(crate) async fn connections_per_client_builder(
    clients: Clients,
    clients_events: ClientsEvents,
    user_to_user_events_counter: WsUserEventsCounter,
    config: AppConfigStatistic,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("clients" / Uuid)
        .and(warp::get())
        .and(warp::any().map(move || clients.clone()))
        .and(warp::any().map(move || clients_events.clone()))
        .and(warp::any().map(move || user_to_user_events_counter.clone()))
        .and(warp::header::optional("Secret-Key"))
        .and(warp::any().map(move || config.routes_secret.clone()))
        .and_then(handler::get_client_connections_handler)
}

pub(crate) async fn get_mps_builder(
    counter: WsRpsCounter,
    config: AppConfigStatistic,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("stat" / "mps" / usize)
        .and(warp::get())
        .and(warp::any().map(move || counter.clone()))
        .and(warp::header::optional("Secret-Key"))
        .and(warp::any().map(move || config.routes_secret.clone()))
        .and_then(handler::get_mps_handler)
}

pub(crate) async fn get_users_stat_builder(
    clients: Clients,
    max_connections: WsMaxConnectionsType,
    config: AppConfigStatistic,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("stat" / "clients")
        .and(warp::get())
        .and(warp::any().map(move || clients.clone()))
        .and(warp::any().map(move || max_connections.clone()))
        .and(warp::header::optional("Secret-Key"))
        .and(warp::any().map(move || config.routes_secret.clone()))
        .and_then(handler::get_clients_count_handler)
}

pub(crate) async fn events_counter_builder(
    counter: WsEventsCounter,
    config: AppConfigStatistic,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("stat" / "events")
        .and(warp::get())
        .and(warp::any().map(move || counter.clone()))
        .and(warp::header::optional("Secret-Key"))
        .and(warp::any().map(move || config.routes_secret.clone()))
        .and_then(handler::get_events_counter)
}

pub(crate) async fn get_daemon_uptime_builder(
    starting_time: usize,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("uptime")
        .and(warp::get())
        .and(warp::any().map(move || starting_time.clone()))
        .map(|starting_time| {
            Response::builder()
                .status(StatusCode::OK)
                .header("content-type", "text/html; charset=utf-8")
                .body(seconds_to_dhms(get_timestamp() - starting_time))
        })
}

const NOT_FOUND_CONTENT: &str = r#"<!DOCTYPE html>
<html lang="en">
<head><title>404 Not Found</title></head>
<body><h1 style="text-align: center">404 Not Found</h1></body>
</html>"#;
