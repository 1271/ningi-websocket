use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::process::exit;
use std::result::Result;
use std::sync::Arc;

use tokio::sync::{mpsc::UnboundedSender, RwLock};
use uuid::Uuid;
use warp::{ws::Message, Rejection};

use ningi_ws_utils::{
    config_struct::*,
    utils::{config, counters_util, logger_util, redis_util},
};

use crate::error_exit;

pub(crate) mod args;
mod jwt_struct;
pub(crate) mod ningi_macro;
mod server;
mod ws;

type WsResult<T> = Result<T, Rejection>;
type Clients = Arc<RwLock<HashMap<String, Client>>>;
type ClientsEvents = Arc<RwLock<HashMap<Uuid, ClientEvents>>>;
type WsMaxConnectionsType = Arc<RwLock<WsMaxClients>>;

pub(crate) const MAX_CONNECTIONS_PER_USER: usize = 4; // максимальное одновременное количество соединений для 1 пользователя (дальнейшие соединения будут обрываться с ошибкой)
pub(crate) const USER_TO_USER_TOPIC: &str = "USER-TO-USER"; // специальный топик, который разрешает получение сообщений от других пользователей
pub(crate) const OFFLINE_MESSAGES_TOPIC: &str = "OFFLINE-MESSAGES-SYSTEM"; // специальный топик, который занимается доставкой оффлайн сообщений пользователям

#[derive(Clone, Debug)]
pub(crate) struct Client {
    pub(crate) user_id: Uuid,
    pub(crate) topics: Vec<String>,
    pub(crate) sender: Option<UnboundedSender<Result<Message, warp::Error>>>,
    pub(crate) timeout: usize, // timestamp
    pub(crate) token_exp: usize,
    pub(crate) connected_at: usize,
    pub(crate) latest_activity: Option<usize>, // timestamp
    pub(crate) is_connected: bool,
}

#[derive(Clone, Debug)]
pub(crate) struct ClientEvents {
    pub(crate) max_connections: usize, // максимальное кол-во соединений для клиента
    pub(crate) received_events: usize, // сообщения, полученные клиентом
    pub(crate) sent_events: usize,     // сообщения, отправленные клиентом
}

impl ClientEvents {
    pub(crate) fn new() -> Self {
        ClientEvents {
            max_connections: 1,
            received_events: 0,
            sent_events: 0,
        }
    }
}

impl Client {
    pub(crate) fn exp_at(&self) -> usize {
        self.timeout + self.connected_at
    }
    pub(crate) async fn send(self, message: ws::ws_structures::WsMessage) {
        if let Some(sender) = self.sender.clone() {
            ws::ws_utils::_send(sender, message).await;
        }
    }
    pub(crate) async fn send_user_message(self, message: ws::ws_structures::WsUserToUserMessage) {
        if let Some(sender) = self.sender.clone() {
            ws::ws_utils::_send_user(sender, message).await;
        }
    }
}

#[derive(Clone, Copy, Default, Debug)]
pub(crate) struct WsMaxClients {
    max_connections: usize,
    max_clients: usize,
}

fn load_config(path: Option<&str>) -> AppConfig {
    match config::load_config(match path {
        Some(path) if !path.ends_with(".toml") => error_exit!("Please pass the path to toml file."),
        Some(path) => path,
        _ => "app.toml",
    }) {
        Ok(config) => config,
        Err(e) => error_exit!("Cannot load config file! Exit.\n{}", e.msg),
    }
}

pub(crate) async fn prepare(config_path: Option<&str>) -> AppConfig {
    let config = load_config(config_path);
    match logger_util::init_logger(config.daemon.log_location.clone()).await {
        Ok(_) => {
            println!("success")
        }
        Err(_) => error_exit!("Failed load config. Exit."),
    };
    config
}

pub(crate) async fn app(config: AppConfig) {
    let messages_counter = counters_util::init_rps_counter().await;
    let events_counter = counters_util::init_events_counter().await;
    let user_events_counter = counters_util::init_user_events_counter().await;

    let jwt_bytes = get_jwt_bytes(config.clone()).await;

    let redis_client = redis_util::connect(config.redis.clone()).await;

    let clients: Clients = Arc::new(RwLock::new(HashMap::new()));
    let clients_events: ClientsEvents = Arc::new(RwLock::new(HashMap::new()));

    let max_connections: WsMaxConnectionsType = Arc::new(RwLock::new(WsMaxClients::default()));

    auto_remove_bad_connections(Arc::clone(&clients)).await;
    auto_disconnect_bad_clients(
        Arc::clone(&clients),
        config.connection.max_idle_time.clone(),
    )
    .await;

    auto_actualize_items(redis_client.clone(), config.redis.clone()).await;

    server::run_server(
        clients,
        jwt_bytes.clone(),
        messages_counter.clone(),
        events_counter.clone(),
        user_events_counter.clone(),
        max_connections,
        redis_client.clone(),
        clients_events.clone(),
        config.clone(),
    )
    .await
}

async fn get_jwt_bytes(config: AppConfig) -> Vec<u8> {
    let jwt_path = config.daemon.jwt_public_key.clone();

    log::debug!(r#"Use JWT key "{}""#, jwt_path);

    let mut jwt_key_file = match File::open(jwt_path) {
        Ok(o) => o,
        Err(_) => error_exit!("JWT key file cannot be open"),
    };

    let buffer = &mut Vec::new();
    match jwt_key_file.read_to_end(buffer) {
        Ok(_) => {}
        Err(_) => error_exit!("JWT key file cannot be read!"),
    };

    let buffer_vec = buffer.to_vec();
    buffer_vec
}

async fn auto_remove_bad_connections(clients: Clients) {
    let _cloned_clients = Arc::clone(&clients);
    tokio::spawn(server::auto_clear_dead_connections_loop(
        _cloned_clients.clone(),
    ));
}

async fn auto_disconnect_bad_clients(clients: Clients, max_idle_time: u8) {
    let _cloned_clients = Arc::clone(&clients);
    tokio::spawn(server::auto_disconnect_loop(
        _cloned_clients.clone(),
        max_idle_time.clone(),
    ));
}

async fn auto_actualize_items(
    client: Option<redis_util::RedisClient>,
    config: Option<AppConfigRedis>,
) {
    if config.is_none() {
        return;
    }

    if let Some(client) = client {
        let _cloned_client = Arc::clone(&client.clone());
        tokio::spawn(redis_util::auto_actualize_items(
            _cloned_client.clone(),
            config.clone().unwrap(),
        ));
    }
}
