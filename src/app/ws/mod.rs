use futures::{FutureExt, StreamExt};
use tokio::sync::mpsc::UnboundedSender;
use tokio::{sync::mpsc, task};
use tokio_stream::wrappers::UnboundedReceiverStream;
use uuid::Uuid;
use warp::ws::{Message, WebSocket};

use ningi_ws_utils::config_struct::AppConfigConnection;
use ningi_ws_utils::{config_struct, utils};

use crate::app::{
    ws::ws_structures::WsMessageTrait, Client, ClientEvents, Clients, ClientsEvents,
    WsMaxConnectionsType,
};

mod ws_messages;
pub(in crate::app) mod ws_structures;
pub(in crate::app) mod ws_utils;

pub(crate) async fn client_connection(
    ws: WebSocket,
    connection_id: Uuid,
    clients: Clients,
    mut client: Client,
    config: config_struct::AppConfig,
    max_connections: WsMaxConnectionsType,
    redis_client: Option<utils::redis_util::RedisClient>,
    clients_events: ClientsEvents,
    user_events_counter: utils::counters_util::WsUserEventsCounter,
) {
    let allowed_topics = config.daemon.topics.clone();

    let (client_ws_sender, mut client_ws_rcv) = ws.split();
    let (client_sender, client_rcv) = mpsc::unbounded_channel();

    let stream = UnboundedReceiverStream::new(client_rcv);

    task::spawn(stream.forward(client_ws_sender).map(|result| {
        if let Err(e) = result {
            log::warn!("{}", e)
        }
    }));

    match success_connected(
        clients.clone(),
        connection_id.clone(),
        client_sender.clone(),
    )
    .await
    {
        Ok(_) => {}
        Err(_) => {
            let _ = clients
                .clone()
                .write()
                .await
                .remove(&connection_id.clone().to_string());

            return;
        }
    }

    let timestamp = utils::get_timestamp();

    client.sender = Some(client_sender);
    client.connected_at = timestamp;
    client.is_connected = true;
    client.latest_activity = Some(timestamp);

    clients
        .write()
        .await
        .insert(connection_id.clone().to_string(), client.clone());

    log::info!("{} connected", connection_id.clone());

    check_max_client_connections(
        clients_events.clone(),
        clients.clone(),
        client.user_id.clone(),
    )
    .await;

    ws_utils::system_stat(clients.clone(), max_connections).await;
    ws_utils::delivery_offline_messages(redis_client, config.redis.clone(), client.clone()).await;

    while let Some(result) = client_ws_rcv.next().await {
        if let Some(c) = clients.read().await.get(&connection_id.clone().to_string()) {
            if let Some(la) = c.latest_activity {
                let idle_time = config.connection.max_idle_time as usize;
                if la + idle_time < timestamp {
                    log::warn!("Interrupted receiving message for inactive client.");

                    if let Some(s) = c.clone().sender {
                        s.send(Ok(Message::close_with(
                            1u16,
                            String::from("Disconnected for inactivity"),
                        )))
                        .ok();
                    }

                    break;
                }
            }
        }

        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                log::error!(
                    r#"Error receiving ws message for id "{}": {}"#,
                    connection_id.clone(),
                    e.to_string()
                );
                break;
            }
        };

        let msg_result = client_msg(
            connection_id.clone(),
            msg,
            &clients.clone(),
            allowed_topics.clone(),
            clients_events.clone(),
            user_events_counter.clone(),
            config.connection.clone(),
        )
        .await;

        match msg_result {
            Ok(r) => log::info!("{}", r),
            Err(r) => log::warn!("{}", r),
        }
    }

    clients
        .write()
        .await
        .remove(&*connection_id.clone().to_string());

    log::info!("{} disconnected", connection_id)
}

async fn client_msg(
    uid_str: Uuid,
    msg: Message,
    clients: &Clients,
    allowed_topics: Option<Vec<String>>,
    clients_events: ClientsEvents,
    user_events_counter: utils::counters_util::WsUserEventsCounter,
    connection_config: AppConfigConnection,
) -> Result<String, String> {
    let mut client = match clients.read().await.get(&uid_str.clone().to_string()) {
        Some(client) if client.is_connected => client.clone(),
        _ => {
            return Err(format!("Client not exists: {}", uid_str.clone()));
        }
    };

    let max_idle_time = connection_config.max_idle_time as usize;
    let timestamp = utils::get_timestamp();

    if let Some(c) = clients.write().await.get_mut(&uid_str.clone().to_string()) {
        c.latest_activity = Some(timestamp);
    };

    let client_sender = match client.sender.clone() {
        Some(sender)
            if client
                .latest_activity
                .unwrap_or(client.connected_at.clone())
                + max_idle_time
                > utils::get_timestamp() =>
        {
            sender
        }
        Some(sender) => {
            log::info!(
                "Client operation timed out. Latest ping from client: {}",
                (match client.latest_activity {
                    Some(t) => format!("{}", t),
                    _ => String::from("newer"),
                })
            );

            ws_utils::_send(
                sender.clone(),
                ws_structures::WsMessage::error(
                    "Operation timed out. Please don't forget to send messages of presence (ping)",
                ),
            )
            .await;

            client.is_connected = false;

            return Err(format!("Operation timed out: {}", uid_str.clone()));
        }
        _ => {
            return Err(format!("Client sender is null: {}", uid_str.clone()));
        }
    };

    let some_client_uid = client.user_id.clone();

    log::debug!("Received message from {}: {:?}", uid_str.clone(), msg);

    if msg.is_pong() {
        return Ok(format!("Pong: {}", uid_str.clone()));
    }

    if msg.is_ping() {
        let _ = client_sender
            .clone()
            .send(Ok(Message::pong(msg.as_bytes())));

        return Ok(format!("Ping: {}", uid_str.clone()));
    };

    if msg.is_text() {
        let message = if let Ok(msg_) = msg.to_str() {
            msg_
        } else {
            let _ = ws_utils::_send(
                client_sender.clone(),
                ws_structures::WsMessage::error("Bad message content"),
            )
            .await;

            return Err(format!("bad user message content: {}", &uid_str.clone()));
        };

        if message == "ping" {
            // т.к. javascript тупой и не умеет отправлять нормальные фреймы, проверяем на простой текст и отправляем обратно "pong"
            let _ = client_sender.clone().send(Ok(Message::text("pong")));

            return Ok(format!("Ping/js: {}", uid_str.clone()));
        }

        // обновляем топики
        match ws_messages::try_update_topics(
            uid_str.clone(),
            message,
            &clients.clone(),
            allowed_topics.clone(),
        )
        .await
        {
            Ok(_) => {
                update_events_counter(clients_events.clone(), some_client_uid.clone()).await;
                return Ok(format!("Success topics update: {}", uid_str.clone()));
            }
            Err(_) => {}
        };

        // отправляем сообщение другим пользователям
        match ws_messages::try_send_user_to_user_message(
            uid_str.clone(),
            message,
            &clients.clone(),
            user_events_counter.clone(),
            some_client_uid.clone(),
        )
        .await
        {
            Ok(_) => {
                update_events_counter(clients_events.clone(), some_client_uid.clone()).await;
                return Ok(format!("Success user-to-user message: {}", uid_str.clone(),));
            }
            Err(_) => {}
        };
    };

    if msg.is_close() {
        let _ = clients
            .clone()
            .write()
            .await
            .remove(&uid_str.clone().to_string());

        return Ok(format!("Success close: {}", uid_str.clone()));
    }

    let _ = ws_utils::_send(
        client_sender.clone(),
        ws_structures::WsMessage::error("Unsupported message type"),
    )
    .await;

    Err(format!(
        "error while parsing user message: {}",
        uid_str.clone()
    ))
}

async fn update_events_counter(clients_events: ClientsEvents, client_id: Uuid) {
    match clients_events.write().await.get_mut(&client_id) {
        Some(c) => {
            c.sent_events += 1;
        }
        None => {}
    };
}

async fn check_max_client_connections(
    clients_events: ClientsEvents,
    clients: Clients,
    user_id: Uuid,
) {
    if clients_events
        .clone()
        .read()
        .await
        .get(&user_id.clone())
        .is_none()
    {
        clients_events
            .clone()
            .write()
            .await
            .insert(user_id.clone(), ClientEvents::new());
        return;
    }

    let current_connections = clients
        .read()
        .await
        .iter()
        .filter(|(_, c)| c.user_id.clone() == user_id.clone())
        .count();

    let max_stored_connections = clients_events
        .clone()
        .read()
        .await
        .get(&user_id.clone())
        .unwrap()
        .max_connections;

    if current_connections > max_stored_connections {
        clients_events
            .clone()
            .write()
            .await
            .get_mut(&user_id.clone())
            .unwrap()
            .max_connections = current_connections;
    }
}

async fn success_connected(
    clients: Clients,
    connection_id: Uuid,
    client_sender: UnboundedSender<Result<Message, warp::Error>>,
) -> Result<(), ()> {
    let timestamp = utils::get_timestamp();

    let clients_ = clients.read().await;
    let some_client = clients_.get(&connection_id.clone().to_string());

    if let Some(client_) = some_client.clone() {
        if client_.clone().sender.is_none() {
            if client_.clone().exp_at() < timestamp {
                log::info!(
                    "Connection with expired token for user {}",
                    client_.clone().user_id.clone()
                );

                let _ = ws_utils::_send(
                    client_sender.clone(),
                    ws_structures::WsMessage::error("Connection expired"),
                )
                .await;
            } else {
                let _ = ws_utils::_send(
                    client_sender.clone(),
                    ws_structures::WsMessage::ok("Connected"),
                )
                .await;

                log::info!(
                    "Success connected for user {}",
                    client_.clone().user_id.clone()
                );

                return Ok(());
            }
        } else {
            log::info!(
                "Double connection for user {}",
                client_.clone().user_id.clone()
            );

            let _ = ws_utils::_send(
                client_sender.clone(),
                ws_structures::WsMessage::error("User already connected"),
            )
            .await;
        }

        return Err(());
    }

    return Ok(());
}
