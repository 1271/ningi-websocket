use itertools::Itertools;
use serde_json::{from_str, to_string};
use tokio::sync::mpsc::UnboundedSender;
use warp::ws::Message;

use crate::app::{
    ws::ws_structures::{OfflineMessage, WsMessage, WsMessageTrait, WsUserToUserMessage},
    Client, Clients, WsMaxConnectionsType,
};
use ningi_ws_utils::{
    config_struct::AppConfigRedis,
    utils::{redis_util, redis_util::SavedMessagesList},
};

pub(in crate::app::ws) async fn system_stat(
    clients: Clients,
    max_connections: WsMaxConnectionsType,
) {
    let actual_connections = clients.read().await.iter().count();
    let actual_clients = clients
        .read()
        .await
        .iter()
        .map(|(_, c)| c.user_id)
        .unique()
        .count();

    if max_connections.read().await.max_connections < actual_connections {
        max_connections.write().await.max_connections = actual_connections;
        log::info!("New maximum connections: {}", actual_connections);
    }

    if max_connections.read().await.max_clients < actual_clients {
        max_connections.write().await.max_clients = actual_clients;
        log::info!("New maximum clients: {}", actual_clients);
    }
}

pub(in crate::app::ws) async fn delivery_offline_messages(
    redis_client: Option<redis_util::RedisClient>,
    config: Option<AppConfigRedis>,
    client: Client,
) {
    if config.is_none() {
        return;
    }

    if let Some(r_client) = redis_client {
        let raw_data = redis_util::get_messages(
            r_client,
            config.clone().unwrap(),
            client.user_id.clone().to_string(),
            client.topics.clone(),
        )
        .await;

        let messages: Vec<OfflineMessage> = match from_str::<SavedMessagesList>(raw_data.as_str()) {
            Ok(l) => l
                .items()
                .iter()
                .map(|x| OfflineMessage::from_saved_message(x))
                .collect(),
            Err(e) => {
                log::warn!("{}", e.to_string());
                vec![]
            }
        };

        if let Some(s) = client.clone().sender {
            s.send(Ok(Message::text(
                to_string(&WsMessage::offline_messages(to_string(&messages).unwrap())).unwrap(),
            )))
            .ok();
        }
    }
}

pub(in crate::app) async fn _send(
    sender: UnboundedSender<Result<Message, warp::Error>>,
    message: WsMessage,
) {
    let _ = sender
        .send(Ok(Message::text(to_string(&message).unwrap())))
        .ok();
}
pub(in crate::app) async fn _send_user(
    sender: UnboundedSender<Result<Message, warp::Error>>,
    message: WsUserToUserMessage,
) {
    let _ = sender
        .send(Ok(Message::text(to_string(&message).unwrap())))
        .ok();
}
