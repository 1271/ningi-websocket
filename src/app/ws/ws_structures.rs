use uuid::Uuid;

use crate::app::OFFLINE_MESSAGES_TOPIC;
use ningi_ws_utils::utils::redis_util;

#[derive(Serialize, Debug)]
pub(crate) struct OfflineMessage {
    pub(crate) message: String,
    pub(crate) topic: String,
    pub(crate) timestamp: usize,
    pub(crate) sender_id: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
pub(crate) struct TopicsRequestMessage {
    pub(crate) topics: Vec<String>,
}

#[derive(Deserialize, Serialize, Debug)]
pub(crate) struct UserMessage {
    pub(crate) user_id: Option<Uuid>,
    pub(crate) message: String,
}

#[derive(Serialize, Debug)]
pub(crate) struct WsMessage {
    pub(crate) r#type: String,
    pub(crate) message: String,
}

#[derive(Serialize, Debug)]
pub(crate) struct WsUserToUserMessage {
    pub(crate) r#type: String,
    pub(crate) message: String,
    pub(crate) user_id: Uuid,
}

pub(crate) trait WsMessageTrait<T> {
    fn ok(_: T) -> Self;
    fn offline_messages(_: T) -> Self;
    fn topics(_: T) -> Self;
    fn error(_: T) -> Self;
}

pub(in crate::app::ws) trait WsUserToUserMessageTrait<T, I> {
    fn message(_: T, user_id: I) -> Self;
}

impl WsUserToUserMessageTrait<String, Uuid> for WsUserToUserMessage {
    fn message(message: String, user_id: Uuid) -> Self {
        WsUserToUserMessage {
            r#type: "user_messages".to_string(),
            message,
            user_id,
        }
    }
}

impl WsMessageTrait<&str> for WsMessage {
    fn ok(message: &str) -> WsMessage {
        WsMessage {
            r#type: String::from("ok"),
            message: String::from(message),
        }
    }
    fn offline_messages(message: &str) -> Self {
        WsMessage {
            r#type: String::from(OFFLINE_MESSAGES_TOPIC),
            message: String::from(message),
        }
    }
    fn topics(message: &str) -> WsMessage {
        WsMessage {
            r#type: String::from("topics"),
            message: String::from(message),
        }
    }
    fn error(message: &str) -> WsMessage {
        WsMessage {
            r#type: String::from("error"),
            message: String::from(message),
        }
    }
}

impl WsMessageTrait<String> for WsMessage {
    fn ok(message: String) -> WsMessage {
        WsMessage {
            r#type: String::from("ok"),
            message,
        }
    }
    fn offline_messages(message: String) -> Self {
        WsMessage {
            r#type: String::from(OFFLINE_MESSAGES_TOPIC),
            message,
        }
    }
    fn topics(message: String) -> WsMessage {
        WsMessage {
            r#type: String::from("topics"),
            message,
        }
    }
    fn error(message: String) -> WsMessage {
        WsMessage {
            r#type: String::from("error"),
            message,
        }
    }
}

impl OfflineMessage {
    pub(crate) fn from_saved_message(message: &redis_util::SavedMessage) -> Self {
        Self {
            message: message.message.clone(),
            topic: message.topic.clone(),
            timestamp: message.timestamp.clone(),
            sender_id: message.sender_id.clone(),
        }
    }
}
