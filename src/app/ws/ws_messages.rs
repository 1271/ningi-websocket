use std::collections::HashMap;

use serde_json::{from_str, to_string};
use uuid::Uuid;

use crate::app::ws::ws_structures;
use crate::app::{
    ws::ws_structures::{WsMessageTrait, WsUserToUserMessage, WsUserToUserMessageTrait},
    Clients, USER_TO_USER_TOPIC,
};
use ningi_ws_utils::utils;

pub(in crate::app::ws) async fn try_update_topics(
    uuid: Uuid,
    message: &str,
    clients: &Clients,
    allowed_topics: Option<Vec<String>>,
) -> Result<(), ()> {
    let string_uid = uuid.to_string();

    match from_str::<ws_structures::TopicsRequestMessage>(message) {
        Ok(topics_req) => {
            let topics = utils::filter_topics(&topics_req.topics, allowed_topics).await;

            if topics
                .clone()
                .iter()
                .filter(|&x| x != &USER_TO_USER_TOPIC.to_string())
                .count()
                == 0
            {
                match clients.read().await.get(&string_uid.clone()) {
                    Some(client) => {
                        client
                            .clone()
                            .send(ws_structures::WsMessage::error(
                                "Topics list cannot be empty",
                            ))
                            .await;
                    }
                    None => {}
                }
            }

            // обновляем топики, если попросили
            match clients.clone().write().await.get_mut(&string_uid.clone()) {
                Some(client) => {
                    client.topics = topics.clone();

                    client
                        .clone()
                        .send(ws_structures::WsMessage::topics(
                            to_string(&topics).unwrap(),
                        ))
                        .await;
                }
                None => {}
            }

            Ok(())
        }
        Err(_) => Err(()),
    }
}

pub(in crate::app::ws) async fn try_send_user_to_user_message(
    uuid: Uuid,
    message: &str,
    clients: &Clients,
    user_events_counter: utils::counters_util::WsUserEventsCounter,
    some_client_uid: Uuid,
) -> Result<(), ()> {
    match from_str::<ws_structures::UserMessage>(message) {
        Ok(user_message) => {
            if user_message.message.clone().len() > 1024 {
                log::info!("User {} send too long message", uuid.clone());

                clients
                    .read()
                    .await
                    .get(&uuid.clone().to_string())
                    .unwrap()
                    .clone()
                    .send(ws_structures::WsMessage::error("Message too long"))
                    .await;

                return Ok(());
            }

            let mut counter = 0usize;
            let mut user_incoming: HashMap<Uuid, usize> = HashMap::new();

            // доставляем сообщение пользователям / пользователю (исключая текущего)
            for (_, client_) in clients.read().await.iter().filter(|(id_, client_)| {
                *id_ != &uuid.clone().to_string()
                    && client_.topics.contains(&USER_TO_USER_TOPIC.to_string())
            }) {
                client_
                    .clone()
                    .send_user_message(WsUserToUserMessage::message(
                        user_message.message.clone(),
                        uuid.clone(),
                    ))
                    .await;

                if some_client_uid != client_.clone().user_id {
                    if let Some(client_mut) = user_incoming.get_mut(&client_.clone().user_id) {
                        *client_mut += 1;
                    } else {
                        user_incoming.insert(client_.clone().user_id, 1);
                    };

                    counter += 1;
                }
            }

            if counter == 0 {
                return Ok(());
            }

            let has_counter = user_events_counter
                .clone()
                .read()
                .await
                .get(&some_client_uid)
                .is_some();

            let user_events_counter_ = user_events_counter.clone();

            // увеличиваем счетчики пользователя
            if has_counter {
                let mut counter_mut = user_events_counter_.write().await;
                let user_counter_mut = counter_mut.get_mut(&some_client_uid).unwrap();

                user_counter_mut.increase_user_to_user_outgoing_real(1);
            } else {
                let mut counter_mut = utils::counters_util::UserEventsCounter::default();
                counter_mut.increase_user_to_user_outgoing_real(1);

                user_events_counter_
                    .write()
                    .await
                    .insert(some_client_uid, counter_mut);
            }

            let user_events_counter_loop = user_events_counter.clone();

            for (uid, inc_to) in user_incoming {
                let has_counter = user_events_counter.clone().read().await.get(&uid).is_some();

                if has_counter {
                    let mut counter_mut = user_events_counter_loop.write().await;

                    let user_counter_mut = counter_mut.get_mut(&uid).unwrap();
                    user_counter_mut.increase_user_to_user_incoming_real(inc_to);
                } else {
                    let mut counter_mut = utils::counters_util::UserEventsCounter::default();
                    counter_mut.increase_user_to_user_incoming_real(inc_to);

                    user_events_counter
                        .clone()
                        .write()
                        .await
                        .insert(uid, counter_mut);
                }
            }

            Ok(())
        }
        Err(_) => Err(()),
    }
}
